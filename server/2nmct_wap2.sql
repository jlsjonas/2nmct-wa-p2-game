-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 10 dec 2013 om 11:02
-- Serverversie: 5.6.13
-- PHP-versie: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `2nmct_wap2`
--
CREATE DATABASE IF NOT EXISTS `2nmct_wap2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `2nmct_wap2`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `scorelist`
--

CREATE TABLE IF NOT EXISTS `scorelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `score` int(4) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Gegevens worden uitgevoerd voor tabel `scorelist`
--

INSERT INTO `scorelist` (`id`, `name`, `score`) VALUES
(1, 'Jonas', 0050),
(2, 'Tom', 0100),
(3, 'Jonas', 0125),
(4, 'Jonas', 0000),
(5, 'Jonas', 0000),
(6, 'Jonas', 0000),
(7, 'Jonas', 0000),
(8, 'Bob', 0000),
(9, 'null', 0000),
(10, 'Jonas', 0125),
(11, 'Bob', 0003),
(12, 'Jonas', 0008),
(13, '', 0026),
(14, 'Jonas', 0037),
(15, '', 0012),
(16, '', 0002),
(17, 'Jonas', 0066),
(18, 'Jonas', 0035),
(19, 'LOSER', 0010),
(20, 'Ben', 0013),
(21, 'Jonas', 0011),
(22, 'null', 0012),
(23, 'Jonas', 0041),
(24, 'Jonas', 0070),
(25, 'null', 0035),
(26, 'null', 0016),
(27, 'null', 0002),
(28, 'null', 0013),
(29, 'Jonas', 0007);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
