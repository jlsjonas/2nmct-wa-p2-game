/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
   fonts['aclonica, sans-serif']='<script src=\"http://use.edgefonts.net/aclonica:n4:all.js\"></script>';


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Gamebg',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'startMenu',
            type:'rect',
            rect:['101px','584px','auto','auto','auto','auto']
         },
         {
            id:'score',
            display:'none',
            type:'text',
            rect:['0','753px','auto','auto','auto','auto'],
            text:"SCORE:",
            align:"center",
            font:['Arial, Helvetica, sans-serif',40,"rgba(122,157,225,1.00)","bold","none","normal"],
            textShadow:["rgba(0,103,158,0.96)",3,3,10]
         },
         {
            id:'lives',
            display:'none',
            type:'text',
            rect:['28px','706px','359px','auto','auto','auto'],
            text:"LIVES:",
            align:"left",
            font:['Arial, Helvetica, sans-serif',40,"rgba(122,157,225,1.00)","bold","none","normal"],
            textShadow:["rgba(0,103,158,0.96)",3,3,10]
         },
         {
            id:'circlePos',
            type:'group',
            rect:['-116px','0px','116','116','auto','auto'],
            c:[
            {
               id:'blink',
               type:'rect',
               rect:['58px','57px','auto','auto','auto','auto']
            },
            {
               id:'circle',
               type:'rect',
               rect:['0px','0px','auto','auto','auto','auto'],
               cursor:['crosshair']
            }]
         },
         {
            id:'heartPos',
            type:'group',
            rect:['76px','639px','161px','161px','auto','auto'],
            c:[
            {
               id:'heart',
               type:'rect',
               rect:['0px','0px','auto','auto','auto','auto']
            }]
         },
         {
            id:'intro',
            display:'none',
            type:'rect',
            rect:['5','0','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'circle',
            symbolName:'circle'
         },
         {
            id:'blink',
            symbolName:'blink'
         },
         {
            id:'intro',
            symbolName:'intro'
         },
         {
            id:'Gamebg',
            symbolName:'Gamebg'
         },
         {
            id:'heart',
            symbolName:'heart'
         },
         {
            id:'startMenu',
            symbolName:'StartMenu'
         }
         ]
      },
   states: {
      "Base State": {
         "${_score}": [
            ["subproperty", "textShadow.blur", '10px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["style", "top", '753px'],
            ["subproperty", "textShadow.offsetV", '3px'],
            ["color", "color", 'rgba(122,157,225,1.00)'],
            ["subproperty", "textShadow.color", 'rgba(0,103,158,0.96)'],
            ["style", "display", 'none'],
            ["style", "font-size", '40px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '800px'],
            ["style", "max-width", '480px'],
            ["style", "width", '480px']
         ],
         "${_intro}": [
            ["style", "display", 'none']
         ],
         "${_heart}": [
            ["transform", "scaleX", '1'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["transform", "scaleY", '1']
         ],
         "${_heartPos}": [
            ["style", "top", '639px'],
            ["style", "height", '161px'],
            ["style", "left", '76px'],
            ["style", "width", '161px']
         ],
         "${_blink}": [
            ["style", "top", '57px'],
            ["style", "right", 'auto'],
            ["style", "left", '58px'],
            ["style", "bottom", 'auto']
         ],
         "${_circle}": [
            ["style", "top", '0px'],
            ["style", "cursor", 'crosshair'],
            ["style", "left", '0px'],
            ["style", "overflow", 'visible']
         ],
         "${_lives}": [
            ["subproperty", "textShadow.blur", '10px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["color", "color", 'rgba(122,157,225,1.00)'],
            ["subproperty", "textShadow.offsetV", '3px'],
            ["style", "left", '28px'],
            ["style", "font-size", '40px'],
            ["style", "top", '706px'],
            ["style", "text-align", 'left'],
            ["style", "display", 'none'],
            ["subproperty", "textShadow.color", 'rgba(0,103,158,0.96)'],
            ["style", "width", '359px']
         ],
         "${_circlePos}": [
            ["style", "left", '-116px'],
            ["style", "top", '0px']
         ],
         "${_txtInfo}": [
            ["style", "font-size", '85px']
         ],
         "${_startMenu}": [
            ["motion", "location", '341px 984px'],
            ["style", "opacity", '1']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 5000,
         autoPlay: true,
         timeline: [
            { id: "eid31", tween: [ "style", "${_blink}", "left", '58px', { fromValue: '58px'}], position: 0, duration: 0 },
            { id: "eid148", tween: [ "style", "${_lives}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid58", tween: [ "motion", "${_startMenu}", [[341,984,0,0],[143,964,0,0]]], position: 0, duration: 250 },
            { id: "eid43", tween: [ "motion", "${_startMenu}", [[143,964,0,0],[237,988.35,53.34,-310.16]]], position: 250, duration: 250 },
            { id: "eid59", tween: [ "motion", "${_startMenu}", [[237,988.35,35.63,-207.15],[337.07,676,-3.57,-330.57]]], position: 500, duration: 250 },
            { id: "eid60", tween: [ "motion", "${_startMenu}", [[337.07,676,-4.91,-455],[137,736,0,0]]], position: 750, duration: 250 },
            { id: "eid64", tween: [ "motion", "${_startMenu}", [[137,736,0,0],[335,678,0,0]]], position: 1000, duration: 250 },
            { id: "eid66", tween: [ "motion", "${_startMenu}", [[335,678,0,0],[182,589.5,-52.24,-152.88]]], position: 1250, duration: 250 },
            { id: "eid70", tween: [ "motion", "${_startMenu}", [[182,589.5,-32.76,-95.86],[286.05,481,36.53,-96.61]]], position: 1500, duration: 250 },
            { id: "eid71", tween: [ "motion", "${_startMenu}", [[286.05,481,55.93,-147.93],[240,400,0,0]]], position: 1750, duration: 250 },
            { id: "eid147", tween: [ "style", "${_score}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid137", tween: [ "style", "${_intro}", "display", 'none', { fromValue: 'none'}], position: 5000, duration: 0 },
            { id: "eid39", tween: [ "style", "${_blink}", "top", '57px', { fromValue: '57px'}], position: 0, duration: 0 }         ]
      }
   }
},
"circle": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'normal',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'red',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
      c: [
      {
         id: 'heart',
         type: 'image',
         rect: ['0','0','116px','116px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/heart.svg','0px','0px']
      }]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'grey',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'green',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_green}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${_normal}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${_red}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${symbolSelector}": [
            ["style", "height", '116px'],
            ["style", "width", '116px']
         ],
         "${_heart}": [
            ["style", "height", '111px'],
            ["style", "top", '10px'],
            ["style", "left", '4px'],
            ["style", "width", '107px']
         ],
         "${_grey}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '0%'],
            ["style", "width", '117px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 4000,
         autoPlay: false,
         labels: {
            "goCircle": 0
         },
         timeline: [
            { id: "eid98", tween: [ "style", "${_red}", "height", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid116", tween: [ "style", "${_green}", "left", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid12", tween: [ "style", "${_normal}", "height", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid4", tween: [ "style", "${_heart}", "height", '0px', { fromValue: '111px'}], position: 0, duration: 4000 },
            { id: "eid114", tween: [ "style", "${_green}", "top", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid15", tween: [ "style", "${_normal}", "top", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid106", tween: [ "style", "${_grey}", "top", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid118", tween: [ "style", "${_green}", "width", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid3", tween: [ "style", "${_heart}", "width", '0px', { fromValue: '107px'}], position: 0, duration: 4000 },
            { id: "eid13", tween: [ "style", "${_normal}", "width", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid108", tween: [ "style", "${_grey}", "left", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid100", tween: [ "style", "${_red}", "left", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid112", tween: [ "style", "${_green}", "height", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid110", tween: [ "style", "${_grey}", "width", '0px', { fromValue: '117px'}], position: 0, duration: 4000 },
            { id: "eid102", tween: [ "style", "${_red}", "width", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid96", tween: [ "style", "${_red}", "top", '50%', { fromValue: '0%'}], position: 0, duration: 4000 },
            { id: "eid104", tween: [ "style", "${_grey}", "height", '0px', { fromValue: '116px'}], position: 0, duration: 4000 },
            { id: "eid14", tween: [ "style", "${_normal}", "left", '50%', { fromValue: '0%'}], position: 0, duration: 4000 }         ]
      }
   }
},
"StartMenu": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      borderRadius: ['40px 40px','40px 40px','40px 40px','40px 40px'],
      rect: ['25%','40.8%','50%','12.5%','auto','auto'],
      id: 'startButton',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      fill: ['rgba(0,159,0,1.00)'],
      c: [
      {
         rect: ['0%','15.6%','100%','0%','auto','auto'],
         font: ['Arial, Helvetica, sans-serif',62,'rgba(0,0,0,1)','700','none',''],
         id: 'startText',
         text: 'START',
         align: 'center',
         type: 'text'
      }]
   },
   {
      type: 'rect',
      borderRadius: ['40px 40px','40px 40px','40px 40px','40px 40px'],
      id: 'endButton',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      rect: ['74.5%','93.6%','25.5%','6.4%','auto','auto'],
      display: 'none',
      fill: ['rgba(0,159,0,1.00)'],
      c: [
      {
         rect: ['0%','9.6%','100%','0%','auto','auto'],
         font: ['Arial, Helvetica, sans-serif',36,'rgba(0,0,0,1)','700','none',''],
         display: 'none',
         id: 'endText',
         text: 'BACK',
         align: 'center',
         type: 'text'
      }]
   },
   {
      type: 'rect',
      borderRadius: ['40px 40px','40px 40px','40px 40px','40px 40px'],
      rect: ['25%','59.9%','50%','12.5%','auto','auto'],
      id: 'helpButton',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      fill: ['rgba(0,159,0,1.00)'],
      c: [
      {
         rect: ['0%','16.3%','100%','0%','auto','auto'],
         font: ['Arial, Helvetica, sans-serif',62,'rgba(0,0,0,1)','700','none',''],
         id: 'helpText',
         text: 'INTRO',
         align: 'center',
         type: 'text'
      }]
   },
   {
      type: 'rect',
      borderRadius: ['40px 40px','40px 40px','40px 40px','40px 40px'],
      rect: ['25%','50%','50%','12.5%','auto','auto'],
      id: 'scoreButton',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      fill: ['rgba(0,159,0,1.00)'],
      c: [
      {
         rect: ['0%','16.3%','100%','0%','auto','auto'],
         font: ['Arial, Helvetica, sans-serif',62,'rgba(0,0,0,1)','700','none',''],
         id: 'highscoresText',
         text: 'SCORES',
         align: 'center',
         type: 'text'
      }]
   },
   {
      transform: [[0,0],['360']],
      rect: ['0px','143px','480px','auto','auto','auto'],
      font: ['aclonica, sans-serif',45,'rgba(111,58,58,1.00)','700','none','normal'],
      id: 'Text',
      align: 'center',
      text: 'THE<br>REACTION<br>GAME',
      textShadow: ['rgba(255,255,255,1.00)',1,0,16],
      type: 'text'
   },
   {
      type: 'text',
      font: ['Arial, Helvetica, sans-serif',36,'rgba(0,0,0,0.42)','bold','none','normal'],
      display: 'none',
      id: 'gameOver',
      align: 'center',
      text: 'GAME OVER',
      textShadow: ['rgba(0,23,255,0.65)',3,3,3],
      rect: ['18px','196','auto','auto','auto','auto']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_endButton}": [
            ["color", "background-color", 'rgba(0,159,0,1.00)'],
            ["style", "border-top-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "opacity", '0.007813'],
            ["style", "left", '74.5%'],
            ["style", "width", '25.5%'],
            ["style", "top", '93.62%'],
            ["style", "border-bottom-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '6.38%'],
            ["style", "cursor", 'pointer'],
            ["style", "border-top-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "display", 'none']
         ],
         "${_highscoresText}": [
            ["style", "top", '12.21%'],
            ["style", "width", '100%'],
            ["style", "text-align", 'center'],
            ["style", "opacity", '1'],
            ["style", "height", '0%'],
            ["style", "font-weight", '700'],
            ["style", "left", '0%'],
            ["style", "font-size", '40px']
         ],
         "${_helpButton}": [
            ["color", "background-color", 'rgba(0,159,0,1.00)'],
            ["style", "border-top-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "opacity", '1'],
            ["style", "left", '25%'],
            ["style", "width", '50%'],
            ["style", "top", '59.88%'],
            ["style", "border-bottom-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '12.5%'],
            ["style", "cursor", 'pointer'],
            ["style", "border-top-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "display", 'block']
         ],
         "${_startText}": [
            ["style", "font-weight", '700'],
            ["style", "left", '0%'],
            ["style", "font-size", '62px'],
            ["style", "top", '15.6%'],
            ["style", "text-align", 'center'],
            ["style", "height", '0%'],
            ["style", "opacity", '1'],
            ["style", "width", '100%'],
            ["style", "display", 'block']
         ],
         "${_gameOver}": [
            ["subproperty", "textShadow.blur", '3px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["color", "color", 'rgba(0,0,0,0.42)'],
            ["subproperty", "textShadow.offsetV", '3px'],
            ["style", "left", '18px'],
            ["style", "font-size", '73px'],
            ["style", "top", '202px'],
            ["style", "display", 'none'],
            ["subproperty", "textShadow.color", 'rgba(0,23,255,0.65)']
         ],
         "${_endText}": [
            ["style", "opacity", '0.007813'],
            ["style", "left", '0%'],
            ["style", "width", '100%'],
            ["style", "top", '9.57%'],
            ["style", "text-align", 'center'],
            ["style", "height", '0%'],
            ["style", "font-weight", '700'],
            ["style", "font-size", '36px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ],
         "${_startButton}": [
            ["color", "background-color", 'rgba(0,159,0,1.00)'],
            ["style", "border-top-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "opacity", '1'],
            ["style", "left", '25%'],
            ["style", "width", '50%'],
            ["style", "top", '40.75%'],
            ["style", "border-bottom-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '12.5%'],
            ["style", "border-top-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "cursor", 'pointer'],
            ["style", "display", 'block']
         ],
         "${_scoreButton}": [
            ["color", "background-color", 'rgba(0,159,0,1.00)'],
            ["style", "border-top-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "opacity", '1'],
            ["style", "left", '60.68%'],
            ["style", "width", '38.48%'],
            ["style", "top", '93.62%'],
            ["style", "border-bottom-left-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '6.38%'],
            ["style", "display", 'block'],
            ["style", "border-top-right-radius", [40,40], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "cursor", 'pointer']
         ],
         "${_helpText}": [
            ["style", "top", '16.27%'],
            ["style", "font-size", '62px'],
            ["style", "text-align", 'center'],
            ["style", "font-weight", '700'],
            ["style", "height", '0%'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '100%']
         ],
         "${_Text}": [
            ["style", "-webkit-transform-origin", [51.39,44.44], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["color", "color", 'rgba(33,17,53,1.00)'],
            ["style", "font-weight", '700'],
            ["style", "left", '0px'],
            ["style", "font-style", 'normal'],
            ["subproperty", "textShadow.blur", '16px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["transform", "rotateZ", '360deg'],
            ["style", "font-size", '45px'],
            ["style", "top", '59px'],
            ["style", "font-family", 'aclonica, sans-serif'],
            ["style", "text-align", 'center'],
            ["subproperty", "textShadow.offsetV", '0px'],
            ["style", "display", 'block'],
            ["subproperty", "textShadow.color", 'rgba(255,255,255,1.00)'],
            ["style", "opacity", '1'],
            ["style", "width", '480px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: false,
         timeline: [
            { id: "eid68", tween: [ "style", "${_endButton}", "opacity", '1', { fromValue: '0.007813'}], position: 1012, duration: 988 },
            { id: "eid187", tween: [ "style", "${_scoreButton}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid67", tween: [ "style", "${_endText}", "opacity", '1', { fromValue: '0.007813'}], position: 1012, duration: 988 },
            { id: "eid188", tween: [ "style", "${_scoreButton}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid32", tween: [ "style", "${_scoreButton}", "left", '60.68%', { fromValue: '60.68%'}], position: 0, duration: 0 },
            { id: "eid48", tween: [ "style", "${_helpButton}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid42", tween: [ "style", "${_Text}", "opacity", '0.0078125', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid49", tween: [ "style", "${_helpText}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid5", tween: [ "style", "${_Text}", "top", '59px', { fromValue: '59px'}], position: 0, duration: 0 },
            { id: "eid38", tween: [ "style", "${_startButton}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid127", tween: [ "style", "${_helpButton}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid129", tween: [ "style", "${_endButton}", "display", 'none', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid182", tween: [ "style", "${_endButton}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
            { id: "eid186", tween: [ "style", "${_gameOver}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid185", tween: [ "style", "${_gameOver}", "display", 'none', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid184", tween: [ "style", "${_gameOver}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
            { id: "eid126", tween: [ "style", "${_startButton}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid44", tween: [ "style", "${_highscoresText}", "font-size", '40px', { fromValue: '40px'}], position: 0, duration: 0 },
            { id: "eid37", tween: [ "style", "${_startText}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid2", tween: [ "color", "${_Text}", "color", 'rgba(33,17,53,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(33,17,53,1.00)'}], position: 0, duration: 0 },
            { id: "eid189", tween: [ "style", "${_highscoresText}", "opacity", '0.007813', { fromValue: '1'}], position: 0, duration: 1000 },
            { id: "eid50", tween: [ "style", "${_highscoresText}", "top", '12.21%', { fromValue: '12.21%'}], position: 0, duration: 0 },
            { id: "eid33", tween: [ "style", "${_scoreButton}", "width", '38.48%', { fromValue: '38.48%'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "style", "${_scoreButton}", "top", '93.62%', { fromValue: '93.62%'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "style", "${_scoreButton}", "height", '6.38%', { fromValue: '6.38%'}], position: 0, duration: 0 },
            { id: "eid128", tween: [ "style", "${_Text}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid130", tween: [ "style", "${_endText}", "display", 'none', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid181", tween: [ "style", "${_endText}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
            { id: "eid125", tween: [ "style", "${_startText}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 }         ]
      }
   }
},
"Instructions": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','480px','800px','auto','auto'],
      type: 'rect',
      id: 'bgCopy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]]
   },
   {
      id: 'into_circles',
      type: 'rect',
      rect: ['91','236','auto','auto','auto','auto']
   },
   {
      font: ['aclonica, sans-serif',45,'rgba(45,136,53,1.00)','700','none','normal'],
      rect: ['65px','86px','480px','auto','auto','auto'],
      type: 'text',
      align: 'center',
      id: 'txtType',
      text: 'Green',
      textShadow: ['rgba(62,190,74,1.00)',1,0,16],
      transform: [[0,0],['360']]
   },
   {
      font: ['aclonica, sans-serif',45,'rgba(45,136,53,1.00)','700','none','normal'],
      rect: ['65px','86px','480px','auto','auto','auto'],
      type: 'text',
      align: 'center',
      id: 'txtInfo',
      text: 'Let\'s play',
      textShadow: ['rgba(62,190,74,1.00)',1,0,16],
      transform: [[0,0],['360']]
   }],
   symbolInstances: [
   {
      id: 'into_circles',
      symbolName: 'into_circles'
   }   ]
   },
   states: {
      "Base State": {
         "${_into_circles}": [
            ["style", "top", '367px'],
            ["style", "left", '190px'],
            ["style", "display", 'block']
         ],
         "${_txtInfo}": [
            ["style", "-webkit-transform-origin", [51.39,44.44], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["color", "color", 'rgba(45,136,53,1.00)'],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["style", "font-style", 'normal'],
            ["subproperty", "textShadow.blur", '16px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["transform", "rotateZ", '360deg'],
            ["style", "width", '480px'],
            ["style", "top", '228px'],
            ["subproperty", "textShadow.color", 'rgba(62,190,74,1.00)'],
            ["style", "text-align", 'center'],
            ["style", "font-weight", 'bold'],
            ["style", "display", 'block'],
            ["style", "font-family", 'aclonica, sans-serif'],
            ["subproperty", "textShadow.offsetV", '0px'],
            ["style", "font-size", '45px']
         ],
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ],
         "${_bgCopy}": [
            ["style", "top", '0px'],
            ["style", "height", '800px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${_txtType}": [
            ["style", "-webkit-transform-origin", [51.39,44.44], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [51.39,44.44],{valueTemplate:'@@0@@% @@1@@%'}],
            ["color", "color", 'rgba(45,136,53,1.00)'],
            ["subproperty", "textShadow.offsetV", '0px'],
            ["style", "left", '8px'],
            ["style", "font-style", 'normal'],
            ["subproperty", "textShadow.blur", '16px'],
            ["subproperty", "textShadow.offsetH", '1px'],
            ["transform", "rotateZ", '360deg'],
            ["style", "font-size", '45px'],
            ["style", "top", '96px'],
            ["style", "width", '480px'],
            ["style", "text-align", 'center'],
            ["style", "opacity", '1'],
            ["style", "display", 'block'],
            ["style", "font-family", 'aclonica, sans-serif'],
            ["style", "font-weight", 'bold'],
            ["subproperty", "textShadow.color", 'rgba(62,190,74,1.00)']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 7000,
         autoPlay: true,
         labels: {
            "max": 7000
         },
         timeline: [
            { id: "eid223", tween: [ "style", "${_txtType}", "top", '96px', { fromValue: '96px'}], position: 0, duration: 0 },
            { id: "eid20", tween: [ "style", "${_bgCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid219", tween: [ "style", "${_bgCopy}", "display", 'none', { fromValue: 'none'}], position: 7000, duration: 0 },
            { id: "eid220", tween: [ "style", "${_into_circles}", "left", '190px', { fromValue: '190px'}], position: 0, duration: 0 },
            { id: "eid216", tween: [ "style", "${_txtInfo}", "display", 'none', { fromValue: 'block'}], position: 7000, duration: 0 },
            { id: "eid174", tween: [ "style", "${_txtType}", "font-size", '75px', { fromValue: '45px'}], position: 0, duration: 750 },
            { id: "eid175", tween: [ "style", "${_txtType}", "font-size", '45px', { fromValue: '75px'}], position: 750, duration: 750 },
            { id: "eid202", tween: [ "style", "${_txtType}", "font-size", '75px', { fromValue: '45px'}], position: 1500, duration: 750 },
            { id: "eid203", tween: [ "style", "${_txtType}", "font-size", '45px', { fromValue: '75px'}], position: 2250, duration: 750 },
            { id: "eid206", tween: [ "style", "${_txtType}", "font-size", '75px', { fromValue: '45px'}], position: 3000, duration: 750 },
            { id: "eid207", tween: [ "style", "${_txtType}", "font-size", '45px', { fromValue: '75px'}], position: 3750, duration: 750 },
            { id: "eid210", tween: [ "style", "${_txtType}", "font-size", '75px', { fromValue: '45px'}], position: 4500, duration: 750 },
            { id: "eid211", tween: [ "style", "${_txtType}", "font-size", '45px', { fromValue: '75px'}], position: 5250, duration: 750 },
            { id: "eid218", tween: [ "style", "${_into_circles}", "display", 'none', { fromValue: 'block'}], position: 7000, duration: 0 },
            { id: "eid221", tween: [ "style", "${_into_circles}", "top", '367px', { fromValue: '367px'}], position: 0, duration: 0 },
            { id: "eid171", tween: [ "style", "${_txtInfo}", "font-size", '30px', { fromValue: '45px'}], position: 0, duration: 750 },
            { id: "eid172", tween: [ "style", "${_txtInfo}", "font-size", '45px', { fromValue: '30px'}], position: 750, duration: 750 },
            { id: "eid200", tween: [ "style", "${_txtInfo}", "font-size", '30px', { fromValue: '45px'}], position: 1500, duration: 750 },
            { id: "eid201", tween: [ "style", "${_txtInfo}", "font-size", '45px', { fromValue: '30px'}], position: 2250, duration: 750 },
            { id: "eid204", tween: [ "style", "${_txtInfo}", "font-size", '30px', { fromValue: '45px'}], position: 3000, duration: 750 },
            { id: "eid205", tween: [ "style", "${_txtInfo}", "font-size", '45px', { fromValue: '30px'}], position: 3750, duration: 750 },
            { id: "eid208", tween: [ "style", "${_txtInfo}", "font-size", '30px', { fromValue: '45px'}], position: 4500, duration: 750 },
            { id: "eid209", tween: [ "style", "${_txtInfo}", "font-size", '45px', { fromValue: '30px'}], position: 5250, duration: 750 },
            { id: "eid212", tween: [ "style", "${_txtInfo}", "font-size", '85px', { fromValue: '45px'}], position: 6000, duration: 1000 },
            { id: "eid217", tween: [ "style", "${_txtType}", "display", 'none', { fromValue: 'block'}], position: 7000, duration: 0 },
            { id: "eid167", tween: [ "style", "${_txtInfo}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid222", tween: [ "style", "${_txtType}", "left", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid168", tween: [ "style", "${_txtInfo}", "top", '228px', { fromValue: '228px'}], position: 0, duration: 0 }         ]
      }
   }
},
"StartAnimation": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','480px','800px','auto','auto'],
      type: 'rect',
      id: 'bg',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_bg}": [
            ["style", "top", '0px'],
            ["style", "height", '800px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid21", tween: [ "style", "${_bg}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 }         ]
      }
   }
},
"Gamebg": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'bg',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','480px','800px','auto','auto'],
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]]
   },
   {
      font: ['aclonica, sans-serif',24,'rgba(0,0,0,1)','normal','none',''],
      type: 'text',
      display: 'none',
      id: 'bgtxt1',
      text: 'Catch the orbs',
      align: 'center',
      rect: ['122px','227px','228px','86px','auto','auto']
   },
   {
      font: ['aclonica, sans-serif',51,'rgba(42,156,110,1.00)','normal','none','normal'],
      type: 'text',
      display: 'none',
      id: 'bgtxt2',
      text: 'before they escape',
      align: 'center',
      rect: ['40','452','auto','auto','auto','auto']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_bg}": [
            ["style", "height", '800px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_bgtxt1}": [
            ["color", "color", 'rgba(255,253,253,1.00)'],
            ["style", "opacity", '0'],
            ["style", "left", '6px'],
            ["style", "font-size", '51px'],
            ["style", "top", '227px'],
            ["style", "text-align", 'center'],
            ["style", "display", 'none'],
            ["style", "font-family", 'aclonica, sans-serif'],
            ["style", "width", '464px']
         ],
         "${_bgtxt2}": [
            ["style", "top", '372px'],
            ["color", "color", 'rgba(42,156,110,1.00)'],
            ["style", "opacity", '0'],
            ["style", "left", '9px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 19126,
         autoPlay: false,
         labels: {
            "bgStart": 0,
            "bgIntro": 1000,
            "bgGameStart": 7000,
            "bgQuickFlashRed": 9000,
            "bgQuickFlash": 10000,
            "bgScores": 11000,
            "bgEndScores": 11250,
            "bg2Left": 12000,
            "bg1Left": 14000,
            "bgGameOver": 16000,
            "bgBackToStart": 18000
         },
         timeline: [
            { id: "eid162", tween: [ "style", "${_bgtxt1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid75", tween: [ "style", "${_bgtxt1}", "display", 'none', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid213", tween: [ "style", "${_bgtxt1}", "display", 'none', { fromValue: 'none'}], position: 1250, duration: 0 },
            { id: "eid79", tween: [ "style", "${_bgtxt1}", "opacity", '1', { fromValue: '0'}], position: 1000, duration: 2000 },
            { id: "eid86", tween: [ "style", "${_bgtxt1}", "opacity", '0.5', { fromValue: '1'}], position: 4000, duration: 2000 },
            { id: "eid90", tween: [ "style", "${_bgtxt1}", "opacity", '0', { fromValue: '0.500000'}], position: 7000, duration: 969 },
            { id: "eid82", tween: [ "style", "${_bgtxt2}", "opacity", '1', { fromValue: '0.000000'}], position: 3000, duration: 2000, easing: "easeInOutCubic" },
            { id: "eid87", tween: [ "style", "${_bgtxt2}", "opacity", '0.5', { fromValue: '1'}], position: 5000, duration: 2000 },
            { id: "eid89", tween: [ "style", "${_bgtxt2}", "opacity", '0', { fromValue: '0.500000'}], position: 7000, duration: 969 },
            { id: "eid80", tween: [ "color", "${_bgtxt1}", "color", 'rgba(42,156,110,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,253,253,1)'}], position: 1000, duration: 2000 },
            { id: "eid85", tween: [ "color", "${_bgtxt1}", "color", 'rgba(31,92,128,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(42,156,110,1)'}], position: 3000, duration: 2000 },
            { id: "eid84", tween: [ "color", "${_bgtxt2}", "color", 'rgba(31,92,128,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(42,156,110,1)'}], position: 3000, duration: 2000 },
            { id: "eid163", tween: [ "style", "${_bgtxt2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid74", tween: [ "style", "${_bgtxt2}", "display", 'none', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid214", tween: [ "style", "${_bgtxt2}", "display", 'none', { fromValue: 'none'}], position: 1250, duration: 0 },
            { id: "eid18", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',47],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]}], position: 0, duration: 1000 },
            { id: "eid34", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',68],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',47],['rgba(54,21,21,1.00)',100]]]}], position: 1000, duration: 4000 },
            { id: "eid35", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',68],['rgba(54,21,21,1.00)',100]]]}], position: 5000, duration: 3000, easing: "easeInOutCubic" },
            { id: "eid164", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(101,36,36,1.00)',0],['rgba(54,21,21,1.00)',86],['rgba(81,44,139,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]]}], position: 9000, duration: 125 },
            { id: "eid165", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(101,36,36,1.00)',0],['rgba(54,21,21,1.00)',86],['rgba(81,44,139,1.00)',100]]]}], position: 9125, duration: 125 },
            { id: "eid61", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]]}], position: 9993, duration: 125 },
            { id: "eid63", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]}], position: 10118, duration: 125 },
            { id: "eid136", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]}], position: 11000, duration: 125 },
            { id: "eid51", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]]}], position: 11250, duration: 125 },
            { id: "eid55", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',68],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',75],['rgba(38,14,14,1.00)',100]]]}], position: 12000, duration: 1000 },
            { id: "eid54", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',47],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',68],['rgba(54,21,21,1.00)',100]]]}], position: 14000, duration: 1000 },
            { id: "eid139", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',0],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',47],['rgba(54,21,21,1.00)',100]]]}], position: 16000, duration: 1126 },
            { id: "eid140", tween: [ "gradient", "${_bg}", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]], { fromValue: [50,50,true,'farthest-corner',[['rgba(14,7,24,1.00)',47],['rgba(54,21,21,1.00)',100]]]}], position: 18000, duration: 1126 }         ]
      }
   }
},
"startButton": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0%','0%','100%','100%','auto','auto'],
      id: 'startButton',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(0,159,0,1.00)']
   },
   {
      rect: ['6%','16.8%','90%','79.2%','auto','auto'],
      id: 'startText',
      text: 'START',
      font: ['Arial, Helvetica, sans-serif',62,'rgba(0,0,0,1)','700','none',''],
      type: 'text'
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_startText}": [
            ["style", "top", '16.8%'],
            ["style", "width", '90%'],
            ["style", "opacity", '1'],
            ["style", "height", '79.2%'],
            ["style", "font-weight", '700'],
            ["style", "left", '6%'],
            ["style", "font-size", '62px']
         ],
         "${_startButton}": [
            ["style", "top", '0%'],
            ["color", "background-color", 'rgba(0,159,0,1.00)'],
            ["style", "height", '100%'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '100%']
         ],
         "${symbolSelector}": [
            ["style", "height", '100px'],
            ["style", "width", '240px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"bonus": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'ellipse',
      borderRadius: ['50%','50%','50%','50%'],
      rect: ['0%','0%','116px','116px','auto','auto'],
      id: 'red',
      stroke: [0,'rgba(0,0,0,1)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      type: 'ellipse',
      borderRadius: ['50%','50%','50%','50%'],
      rect: ['0%','0%','116px','116px','auto','auto'],
      id: 'grey',
      stroke: [0,'rgba(0,0,0,1)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      type: 'ellipse',
      borderRadius: ['50%','50%','50%','50%'],
      rect: ['0%','0%','116px','116px','auto','auto'],
      id: 'green',
      stroke: [0,'rgba(0,0,0,1)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'normal',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_green}": [
            ["style", "top", '50%'],
            ["style", "display", 'none'],
            ["style", "height", '0px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '50%'],
            ["style", "width", '0px']
         ],
         "${_normal}": [
            ["style", "top", '50%'],
            ["style", "display", 'block'],
            ["style", "height", '0px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '50%'],
            ["style", "width", '0px']
         ],
         "${_red}": [
            ["style", "top", '50%'],
            ["style", "display", 'none'],
            ["style", "height", '0px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '50%'],
            ["style", "width", '0px']
         ],
         "${_grey}": [
            ["style", "top", '50%'],
            ["style", "display", 'none'],
            ["style", "height", '0px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "left", '50%'],
            ["style", "width", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '116px'],
            ["style", "width", '116px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: false,
         labels: {
            "goCircle": 200
         },
         timeline: [
            { id: "eid124", tween: [ "style", "${_normal}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid97", tween: [ "style", "${_red}", "height", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid98", tween: [ "style", "${_red}", "height", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid115", tween: [ "style", "${_green}", "left", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid116", tween: [ "style", "${_green}", "left", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid121", tween: [ "style", "${_green}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid92", tween: [ "style", "${_normal}", "height", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid12", tween: [ "style", "${_normal}", "height", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid113", tween: [ "style", "${_green}", "top", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid114", tween: [ "style", "${_green}", "top", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid91", tween: [ "style", "${_normal}", "top", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid15", tween: [ "style", "${_normal}", "top", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid105", tween: [ "style", "${_grey}", "top", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid106", tween: [ "style", "${_grey}", "top", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid122", tween: [ "style", "${_grey}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid94", tween: [ "style", "${_normal}", "width", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid13", tween: [ "style", "${_normal}", "width", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid93", tween: [ "style", "${_normal}", "left", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid14", tween: [ "style", "${_normal}", "left", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid95", tween: [ "style", "${_red}", "top", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid96", tween: [ "style", "${_red}", "top", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid123", tween: [ "style", "${_red}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid101", tween: [ "style", "${_red}", "width", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid102", tween: [ "style", "${_red}", "width", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid111", tween: [ "style", "${_green}", "height", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid112", tween: [ "style", "${_green}", "height", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid99", tween: [ "style", "${_red}", "left", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid100", tween: [ "style", "${_red}", "left", '50%', { fromValue: '0%'}], position: 200, duration: 800 },
            { id: "eid109", tween: [ "style", "${_grey}", "width", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid110", tween: [ "style", "${_grey}", "width", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid117", tween: [ "style", "${_green}", "width", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid118", tween: [ "style", "${_green}", "width", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid103", tween: [ "style", "${_grey}", "height", '116px', { fromValue: '0px'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid104", tween: [ "style", "${_grey}", "height", '0px', { fromValue: '116px'}], position: 200, duration: 800 },
            { id: "eid107", tween: [ "style", "${_grey}", "left", '0%', { fromValue: '50%'}], position: 0, duration: 200, easing: "easeInOutBounce" },
            { id: "eid108", tween: [ "style", "${_grey}", "left", '50%', { fromValue: '0%'}], position: 200, duration: 800 }         ]
      }
   }
},
"heart": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'heart2',
      type: 'image',
      rect: ['0px','0px','161px','161px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/heart.svg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '161px'],
            ["style", "width", '161px']
         ],
         "${_heart2}": [
            ["style", "top", '0px'],
            ["style", "display", 'block'],
            ["style", "height", '38px'],
            ["style", "left", '0px'],
            ["style", "width", '38px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         timeline: [
            { id: "eid16", tween: [ "style", "${_heart2}", "width", '161px', { fromValue: '38px'}], position: 0, duration: 500 },
            { id: "eid143", tween: [ "style", "${_heart2}", "width", '100px', { fromValue: '161px'}], position: 500, duration: 500 },
            { id: "eid144", tween: [ "style", "${_heart2}", "left", '31px', { fromValue: '0px'}], position: 500, duration: 500 },
            { id: "eid10", tween: [ "style", "${_heart2}", "height", '161px', { fromValue: '38px'}], position: 0, duration: 500 },
            { id: "eid142", tween: [ "style", "${_heart2}", "height", '100px', { fromValue: '161px'}], position: 500, duration: 500 },
            { id: "eid146", tween: [ "style", "${_heart2}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid145", tween: [ "style", "${_heart2}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid141", tween: [ "style", "${_heart2}", "top", '-181px', { fromValue: '0px'}], position: 500, duration: 500 }         ]
      }
   }
},
"scores": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      rect: ['0px','0px','480px','800px','auto','auto'],
      id: 'bg',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]]
   },
   {
      rect: ['-2px','0px','480px','75px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',65,'rgba(0,0,0,1)','bold','none','normal'],
      align: 'center',
      id: 'txtHeader',
      text: 'HIGH SCORES',
      textShadow: ['rgba(42,29,239,0.65)',3,3,3],
      type: 'text'
   },
   {
      type: 'text',
      font: ['Arial, Helvetica, sans-serif',65,'rgba(0,0,0,1)','bold','none','normal'],
      cursor: ['pointer'],
      id: 'txtReturn',
      align: 'center',
      text: 'RETURN',
      textShadow: ['rgba(42,29,239,0.65)',3,3,3],
      rect: ['-2px','725px','480px','75px','auto','auto']
   },
   {
      rect: ['107px','86px','77.5%','82.8%','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',40,'rgba(220,201,236,1.00)','bold','none','normal'],
      align: 'left',
      id: 'txtScores',
      text: 'Loading...',
      textShadow: ['rgba(56,40,70,0.92)',3,3,15],
      type: 'text'
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_bg}": [
            ["style", "top", '0px'],
            ["style", "height", '800px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,44,139,1.00)',0],['rgba(54,21,21,1.00)',100]]]],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${_txtHeader}": [
            ["subproperty", "textShadow.blur", '3px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["subproperty", "textShadow.offsetV", '3px'],
            ["style", "left", '-2px'],
            ["style", "width", '480px'],
            ["style", "top", '0px'],
            ["style", "height", '75px'],
            ["subproperty", "textShadow.color", 'rgba(42,29,239,0.65)'],
            ["style", "opacity", '0'],
            ["style", "font-size", '65px']
         ],
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ],
         "${_txtReturn}": [
            ["subproperty", "textShadow.blur", '3px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["subproperty", "textShadow.offsetV", '3px'],
            ["style", "left", '-2px'],
            ["style", "width", '480px'],
            ["style", "top", '725px'],
            ["style", "opacity", '0'],
            ["style", "height", '75px'],
            ["subproperty", "textShadow.color", 'rgba(42,29,239,0.65)'],
            ["style", "font-size", '65px'],
            ["style", "cursor", 'pointer']
         ],
         "${_txtScores}": [
            ["subproperty", "textShadow.blur", '15px'],
            ["subproperty", "textShadow.offsetH", '3px'],
            ["color", "color", 'rgba(220,201,236,1.00)'],
            ["style", "opacity", '0'],
            ["style", "left", '107px'],
            ["style", "width", '77.51%'],
            ["style", "top", '88px'],
            ["style", "text-align", 'left'],
            ["style", "height", '79.68%'],
            ["subproperty", "textShadow.color", 'rgba(56,40,70,0.92)'],
            ["subproperty", "textShadow.offsetV", '3px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         labels: {
            "scoresClose": 600
         },
         timeline: [
            { id: "eid62", tween: [ "style", "${_txtScores}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 500 },
            { id: "eid132", tween: [ "style", "${_txtScores}", "opacity", '0', { fromValue: '1'}], position: 600, duration: 400 },
            { id: "eid69", tween: [ "style", "${_txtReturn}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 500 },
            { id: "eid133", tween: [ "style", "${_txtReturn}", "opacity", '0', { fromValue: '1'}], position: 600, duration: 400 },
            { id: "eid135", tween: [ "style", "${_bg}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid65", tween: [ "style", "${_txtHeader}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 500 },
            { id: "eid134", tween: [ "style", "${_txtHeader}", "opacity", '0', { fromValue: '1'}], position: 600, duration: 400 }         ]
      }
   }
},
"intro": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'Rectangle',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','480px','800px','auto','auto'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '800px'],
            ["style", "width", '480px']
         ],
         "${_Rectangle}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: true,
         timeline: [
            { id: "eid138", tween: [ "style", "${_Rectangle}", "left", '480px', { fromValue: '0px'}], position: 0, duration: 2000 }         ]
      }
   }
},
"blink": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['-10px','-10px','0px','0px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'blink',
      stroke: [10,'rgba(40,20,57,1.00)','solid'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_blink}": [
            ["color", "background-color", 'rgba(56,39,69,0.51)'],
            ["style", "border-style", 'solid'],
            ["style", "border-width", '10px'],
            ["style", "width", '0px'],
            ["style", "top", '-10px'],
            ["style", "height", '0px'],
            ["color", "border-color", 'rgba(40,20,57,1.00)'],
            ["style", "display", 'block'],
            ["style", "left", '-10px']
         ],
         "${symbolSelector}": [
            ["style", "height", '0px'],
            ["style", "width", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 250,
         autoPlay: false,
         timeline: [
            { id: "eid46", tween: [ "style", "${_blink}", "left", '-939px', { fromValue: '-10px'}], position: 0, duration: 250 },
            { id: "eid45", tween: [ "style", "${_blink}", "height", '1857px', { fromValue: '0px'}], position: 0, duration: 250 },
            { id: "eid52", tween: [ "style", "${_blink}", "top", '-939px', { fromValue: '-10px'}], position: 0, duration: 250 },
            { id: "eid53", tween: [ "color", "${_blink}", "background-color", 'rgba(190,167,209,0.10)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(56,39,69,0.51)'}], position: 0, duration: 250 },
            { id: "eid57", tween: [ "style", "${_blink}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid224", tween: [ "style", "${_blink}", "display", 'block', { fromValue: 'block'}], position: 6, duration: 0 },
            { id: "eid56", tween: [ "style", "${_blink}", "display", 'none', { fromValue: 'block'}], position: 250, duration: 0 },
            { id: "eid47", tween: [ "style", "${_blink}", "width", '1857px', { fromValue: '0px'}], position: 0, duration: 250 }         ]
      }
   }
},
"into_circles": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'normal',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'red',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
      c: [
      {
         id: 'heart',
         type: 'image',
         rect: ['0','0','116px','116px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/heart.svg','0px','0px']
      }]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'grey',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   },
   {
      rect: ['0%','0%','116px','116px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'green',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)',[50,50,'true','farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_green}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(45,136,53,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${_normal}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(81,45,136,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${_red}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(136,45,45,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '116px']
         ],
         "${symbolSelector}": [
            ["style", "height", '116px'],
            ["style", "width", '116px']
         ],
         "${_heart}": [
            ["style", "top", '10px'],
            ["style", "display", 'block'],
            ["style", "height", '111px'],
            ["style", "opacity", '1'],
            ["style", "left", '4px'],
            ["style", "width", '107px']
         ],
         "${_grey}": [
            ["style", "top", '0%'],
            ["style", "height", '116px'],
            ["gradient", "background-image", [50,50,true,'farthest-corner',[['rgba(99,99,99,1.00)',10],['rgba(0,0,0,1.00)',100]]]],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["style", "left", '0%'],
            ["style", "width", '117px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 6000,
         autoPlay: true,
         labels: {
            "goCircle": 0
         },
         timeline: [
            { id: "eid81", tween: [ "style", "${_grey}", "display", 'none', { fromValue: 'block'}], position: 3000, duration: 0 },
            { id: "eid83", tween: [ "style", "${_heart}", "display", 'none', { fromValue: 'block'}], position: 4500, duration: 0 },
            { id: "eid152", tween: [ "style", "${_red}", "opacity", '0', { fromValue: '1'}], position: 4250, duration: 250 },
            { id: "eid78", tween: [ "style", "${_green}", "display", 'none', { fromValue: 'block'}], position: 1500, duration: 0 },
            { id: "eid155", tween: [ "style", "${_heart}", "opacity", '0', { fromValue: '1'}], position: 4000, duration: 500 },
            { id: "eid88", tween: [ "style", "${_red}", "display", 'none', { fromValue: 'block'}], position: 4500, duration: 0 },
            { id: "eid149", tween: [ "style", "${_green}", "opacity", '0', { fromValue: '1'}], position: 1250, duration: 250 },
            { id: "eid151", tween: [ "style", "${_grey}", "opacity", '0', { fromValue: '1'}], position: 2750, duration: 250 },
            { id: "eid119", tween: [ "style", "${_normal}", "display", 'none', { fromValue: 'block'}], position: 6000, duration: 0 },
            { id: "eid154", tween: [ "style", "${_normal}", "opacity", '0', { fromValue: '1'}], position: 5750, duration: 250 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-728156");
