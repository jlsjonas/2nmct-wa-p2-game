/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
//var outerColor = rgba(54,21,21,1.00);


(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes
    const GAME_INIT = 0;
    const GAME_START = 1
    const GAME_STARTED = 2;
    const GAME_OVER = 3;
    const GAME_HELP = 4;
    const GAME_SCORE = 5;
    const ORB_NORMAL = 0;
    const ORB_RED = 1;
    const ORB_GREY = 2;
    const ORB_GREEN = 3;
    const API_URL = "http://localhost:8080/web%20advanced/p2_game/server/WAP2api.php";
    var stageRef;
    var circleRef;
    var scoreList;
    var gameState = GAME_INIT;
    
    
    const startRadius = 1; //position on map
    const roundRadius = 0.1;
    var currentRadius;
    //const minRadius = 5;
    const startDuration = 40;
    const roundDuration = 1;
    const minDuration = 25;
    const startLives = 3;
    const normalChance = 4;
    
    const stageWidth = 480;
    const stageHeight = 750;
    var currentRound;
    
    
    
    //position on map
    var xPos;
    var yPos;
    var currentDuration;
    var xDir;
    var yDir;
    var xd,yd;
    
    //round data
    var roundType;
    
    var score;
    var lives;
    //var Position; //timeline position => orb size, 250 ms = biggest
    
    
    function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
	}
    function getRandom (min, max) {
    return Math.random() * (max - min + 1) + min;
	}
    function gameSetup() {
    	
    }
    function gameLoad() {
    	score = 0;
    	currentRound = 0;
    	lives = startLives;
    	currentDuration = startDuration;
    	currentRadius = startRadius;
    	stageRef.$("behindCircle").show();
      stageRef.$("")
    }
    function hideCircles() {
    	console.log("hiding...");
    	stageRef.getSymbol("circle").$("normal").hide();
    	stageRef.getSymbol("circle").$("red").hide();
    	stageRef.getSymbol("circle").$("grey").hide();
    	stageRef.getSymbol("circle").$("green").hide();
      stageRef.getSymbol("blink").$("blink").hide();
    }
    function updateScores() {
    		if (lives < 0) lives = 0;
			stageRef.$("score").text("SCORE: "+score);
			stageRef.$("lives").text("LIVES: "+lives);
    }
    function roundLoad() {
    	if(gameState == GAME_STARTED)
		{
      roundType = getRandomInt(-1*normalChance,3);
			currentRound++;
			currentRadius += roundRadius;
			if(currentDuration > minDuration) currentDuration -= roundDuration;
			updateScores();
    	if(lives < 0)
				gameOver();
			else {
				console.log("roundLoad, lives:"+lives+", currentDuration:"+currentDuration);
				var nxd,nyd;
				do {
					nxd = getRandomInt(0,1);
					if(nxd==0) nxd = -1;
					nyd = getRandomInt(0,1);
					if(nyd==0) nyd = -1;
				} while(nxd==xd && nyd==yd);
				xd = nxd;
				yd = nyd;
				//random initial position
				
				xPos = getRandomInt(116,stageWidth-116);
				yPos = getRandomInt(116,stageWidth-116);
				hideCircles();
				getNewDirection();
				stageRef.$("circlePos").animate({left: xPos, top:yPos},1, moveComplete);
			}
    	} else console.log("skipped round (wrong gameState)");
    }
    function roundLost() {
      if(roundType!=ORB_GREY&&roundType!=ORB_RED) {
        lives--;
        stageRef.getSymbol("Gamebg").play("bgQuickFlashRed");
      } else
        stageRef.getSymbol("Gamebg").play("bgQuickFlash");

      if(lives > 0)
        roundLoad();
      else if(gameState==GAME_STARTED)
        gameOver();
    }
    function getNewDirection() {
    	/*method 1*
    	var rad;
    	do {
			/*xDir = getRandomInt(116,stageWidth-116);
			yDir = getRandomInt(116,stageHeight-116);*
			xDir = getRandomInt(xPos-currentRadius,xPos+currentRadius);
			yDir = getRandomInt(yPos-currentRadius,yPos+currentRadius);
			rad = (xPos - xDir)^2 + (yPos - yDir)^2;
		} while(xDir < 116 || xDir > stageWidth-116 || yDir < 116 || yDir > stageHeight-116 || minRadius > rad); // || rad > currentRadius));
		console.log("radius: "+rad+" current max: "+currentRadius);
		/* end method 1*/
		
		/*method 2*/
    	var xOffset,yOffset;
		xOffset = currentRadius;//getRandomInt(minRadius,currentRadius);
		yOffset = currentRadius;//getRandomInt(minRadius,currentRadius);
			xDir = xPos+(xd*xOffset);
			yDir = yPos+(yd*yOffset);
		if(xDir < 0)
			xd= 1;
		else if(xDir > stageWidth-116)
			xd= -1;
		else if(yDir < 0)
			yd= 1;
		else if(yDir > stageHeight-116)
			yd=-1;
		xDir = xPos+(xd*xOffset);
		yDir = yPos+(yd*yOffset);
		/*end method2*/
	 }
    function newAnimation(cR) {
    	xPos = xDir;
    	yPos = yDir;
		 if(gameState == GAME_STARTED && currentRound==cR) {
			//console.log("new animation x"+xDir+" y"+yDir);
			//stageRef.$("circlePos").animate({left: xDir, top:yDir},currentDuration, newAnimation);
			
			stageRef.$("circlePos").animate({ left: xDir, top:yDir }, currentDuration, 
					(function (z) {
			        return function() {
					  newAnimation(z);
					  }
				 })(currentRound));
			
			
			getNewDirection();
		 } else console.log("skipped newAnimation (wrong gameState/circleCount)");
    }
    function createOrb(rT) {
      switch(rT) {
        case ORB_RED:
          console.log("red");
          stageRef.getSymbol("circle").$("red").show();
          break;
        case ORB_GREY:
          console.log("grey");
          stageRef.getSymbol("circle").$("grey").show();
          break;
        case ORB_GREEN:
          console.log("green");
          stageRef.getSymbol("circle").$("green").show();
          break;
        default : //ORB_NORMAL
          roundType = ORB_NORMAL;
          console.log("normal");
          stageRef.getSymbol("circle").$("normal").show();
          break;
      }
      stageRef.getSymbol("circle").play(0);
      circleRef.play("goCircle");
    }
    function moveComplete() { // pick orb type
			createOrb(roundType);
			stageRef.$("circlePos").animate({ left: xDir, top:yDir }, currentDuration, 
					(function (z) {
					  newAnimation(z);
				 })(currentRound));
			//stageRef.$("circlePos").animate({left: xDir, top:yDir},currentDuration);
    	console.log("round loaded "+roundType+" x:"+xPos+" y:"+yPos);
    	
    }
    
    function spawnHeart() {
    	// Create an instance element of a symbol as a child of the
    	// given parent element
    	//heart = stageRef.createChildSymbol("heart", "Stage");
      //$("heartPos").css({left: xPos, top:yPos});
      stageRef.$("heartPos").animate({left: xDir, top:yDir},1);
      stageRef.$("heartPos").animate({left: 76, top:639},300);
    	stageRef.getSymbol("heart").play(0);
    	//heart.animate({},100,heartCompleted);
    }

    
    function gameOver() {
      if(gameState==GAME_STARTED) {
    	//TODO: write out
    	gameState = GAME_OVER;
    	updateScores();
      pushHighscore()
    	stageRef.$("startMenu").show();
    	//stageRef.$("startMenu").playReverse(2000);
      }
    }

    function pushHighscore() {
      do {
        var name = prompt("You've scores " + score + "\nPlease provide your name.");
      } while(name=="")
      $.getJSON( API_URL+"?api=set&n="+name+"&s="+score, function( data ) {
           var items = [];
           $.each( data, function( i, ns ) {
           items.push(ns);
           });
            // Change an Element's contents.
            //  (sym.$("name") resolves an Edge Animate element name to a DOM
            //  element that can be used with jQuery)
            //sym.$("txtScores").html(items.join("<br />"));
        console.log("HIGHSCORE: " + name + " " + score + " DEBUG:"+items.join(" "));
           /*$( "<ul/>", {
           "class": "my-new-list",
           html: items.join( "" )
           }).appendTo( "body" );*/
         });
      console.log("Requesting push");
    }
    
    function showScores() {
    	//stageRef.getSymbol("Gamebg").play("bgScores");
      gameState = GAME_SCORE;
      //stageRef.getSymbol("startMenu").play(0);
    	scoreList = stageRef.createChildSymbol("scores", "Stage");
    	scoreList.play(0);
    }
    
    function hideScores() {
      gamestate = GAME_INIT;
		  stageRef.getSymbol("Gamebg").play("bgEndScores");
      scoreList.play("scoresClose");
      //stageRef.getSymbol("startMenu").playReverse(2000);

    }


   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         // versimpelen van sym.getComposition().getStage()
         stageRef = sym;

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_Stage}", "click", function(sym, e) {
         // insert code for mouse click here
         //sym.$("startMenu").hide();
         // insert code for mouse click here
         /*if(gameState == GAME_STARTED) {
         	console.log("clicked stage");
         	stageRef.getSymbol("Gamebg").play("bgQuickFlashRed");
            currentRadius += roundRadius;
            currentDuration += roundDuration;
         	roundLoad();
         } else {
         	console.log("hiding circles");
         	hideCircles();
         }*/

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_circle}", "click", function(sym, e) {
      // insert code for mouse click here
      if(gameState == GAME_STARTED) {
       	console.log("clicked circle");
        stageRef.getSymbol("blink").play(0);
       	stageRef.getSymbol("Gamebg").play("bgQuickFlash");
       	//stageRef.getSymbol("circle").stop(0);
       	switch(roundType) {
       	case ORB_NORMAL:
       			score++;
       			break;
       	case ORB_RED:
       			spawnHeart();
       			lives++;
       			break;
       	case ORB_GREEN:
       			score=score+getRandomInt(2,10);
       			break;
       	case ORB_GREY:
            lives--;
       			roundLost();
       			break;
       	}
        if(roundType!=ORB_GREY)
       	  roundLoad();
      } else {
      	console.log("hiding");
      	hideCircles();
      	//sym.$(e.target).hide();
      }

      });
      //Edge binding end

      

      

      Symbol.bindElementAction(compId, symbolName, "${_Gamebg}", "click", function(sym, e) {
        // insert code for mouse click here
        if(gameState == GAME_STARTED) {
            roundLost();
        } else {
          hideCircles();
        }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'circle'
   (function(symbolName) {   
   
      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         // insert code here
         roundLost();

      });
      //Edge binding end

      

      

      

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         // insert code to be run when the symbol is created here
         circleRef = sym;

      });
      //Edge binding end

   })("circle");
   //Edge symbol end:'circle'

   //=========================================================
   
   //Edge symbol: 'START'
   (function(symbolName) {   
   
      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         //startMenuAnimation-end
         //sym.stop();
         stageRef.$("startMenu").stop();
         stageRef.$("startMenu").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_startButton}", "click", function(sym, e) {
         gameState = GAME_START;
         console.log(gameState);
         
         stageRef.getSymbol("startMenu").play(0);
         stageRef.getSymbol("Gamebg").play("bgGameStart");
         //stageRef.getSymbol("Gamebg").play("bgIntro");
         //stageRef.getSymbol("circle").show();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_helpButton}", "click", function(sym, e) {
         // insert code for mouse click here
         stageRef.getSymbol("startMenu").play(0);
         gameState==GAME_HELP;
         stageRef.getSymbol("Gamebg").play("bgIntro");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_endButton}", "click", function(sym, e) {
         // insert code for mouse click here
         stageRef.getSymbol("startMenu").playReverse();
         stageRef.getSymbol("Gamebg").play("bgBackToStart");
         hideCircles();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         // insert code here
         
         if(gameState==GAME_OVER||gameState==GAME_HELP||gameState==GAME_INIT) {
         	if(gameState==GAME_OVER)
         		gameState = GAME_INIT;
         	stageRef.$("startMenu").show();
         	//stageRef.$("Gamebg").play("");
         	stageRef.$("score").hide();
         	stageRef.$("lives").hide();
         }
         else if(gameState==GAME_SCORE) {
         	console.log("startmenutrigger-gamescore");
         	//if(gameState==GAME_SCORE)
         		showScores();
         	stageRef.$("startMenu").hide();
         	stageRef.$("score").hide();
         	stageRef.$("lives").hide();
         } else {
         	console.log("display score/lives");
         	stageRef.$("startMenu").hide();
         	stageRef.$("score").show();
         	stageRef.$("lives").show();
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_scoreButton}", "click", function(sym, e) {
         // insert code for mouse click here
         gameState = GAME_SCORE;
         stageRef.getSymbol("startMenu").play(0);
         //showScores();

      });
      //Edge binding end

   })("StartMenu");
   //Edge symbol end:'StartMenu'

   //=========================================================
   
   //Edge symbol: 'START_1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         // Change an Element's contents.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("txtType").html("Gray");
         sym.$("txtInfo").html("Click OUTSIDE the orb");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         // Change an Element's contents.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("txtType").html("Red/Heart");
         sym.$("txtInfo").html("Extra Life");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4500, function(sym, e) {
         // Change an Element's contents.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("txtType").html("Regular");
         sym.$("txtInfo").html("Regular Orb\n1 point");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6000, function(sym, e) {
         // Change an Element's contents.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("txtType").hide()
         sym.$("txtInfo").hide(); //("Let's play!");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // Change an Element's contents.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("txtType").html("Green");
         sym.$("txtInfo").html("2~10 bonus points");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7000, function(sym, e) {
         gameState = GAME_INIT;
         stageRef.getSymbol("startMenu").playReverse();

      });
      //Edge binding end

   })("Instructions");
   //Edge symbol end:'Instructions'

   //=========================================================
   
   //Edge symbol: 'Instructions_1'
   (function(symbolName) {   
   
   })("StartAnimation");
   //Edge symbol end:'StartAnimation'

   //=========================================================
   
   //Edge symbol: 'StartMenu_1'
   (function(symbolName) {   
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7969, function(sym, e) {
         // insert code here
         sym.$("bgtxt1").hide();
         sym.$("bgtxt2").hide();
         sym.stop();
         if(gameState == GAME_START)
         {
         gameSetup();
         gameLoad();
         gameState = GAME_STARTED;
         roundLoad();
         } else { console.log("Wrong gameState");}

      });
      //Edge binding end
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 10250, function(sym, e) {
         sym.stop();
      
      });
      //Edge binding end
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 13000, function(sym, e) {
         sym.stop();
      
      });
      //Edge binding end
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 15000, function(sym, e) {
         sym.stop();
        gameOver();
         //gameState = GAME_OVER;
      
      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_bg}", "click", function(sym, e) {
         // insert code for mouse click here
      
      });
      //Edge binding end

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 9250, function(sym, e) {
         sym.stop();
         if(lives==2)
         	stageRef.getSymbol("Gamebg").play("bg2Left");
         else if(lives==1)
         	stageRef.getSymbol("Gamebg").play("bg1Left");
         else if (lives<=0)
         	stageRef.getSymbol("Gamebg").play("bgGameOver");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 11375, function(sym, e) {
         sym.stop();
      
      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 11125, function(sym, e) {
         sym.stop();
      
      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 17126, function(sym, e) {
         sym.stop();
         // insert code here
         gameOver();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         // insert code here
         if(gameState=GAME_HELP)
         	gameHelp = sym.createChildSymbol("Instructions", "Stage");
         

      });
      //Edge binding end

   })("Gamebg");
   //Edge symbol end:'Gamebg'

   //=========================================================
   
   //Edge symbol: 'startButton'
   (function(symbolName) {   
   
   })("startButton");
   //Edge symbol end:'startButton'

   //=========================================================
   
   //Edge symbol: 'circle_1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         // insert code here
         roundLost();

      });
         //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_normal}", "click", function(sym, e) {
         // insert code for mouse click here
         //stageRef.getSymbol("Gamebg").play("bgQuickFlash");
         /*if(gameState == GAME_STARTED)
         	score++;
         else
         	console.log("no score, wrong gamestate");
         */

      });
         //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_red}", "click", function(sym, e) {
         // insert code for mouse click here
         stageRef.getSymbol("Gamebg").play("bgQuickFlash");
         if(gameState == GAME_STARTED)
         	lives++;

      });
         //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_grey}", "click", function(sym, e) {
         // insert code for mouse click here
         stageRef.getSymbol("Gamebg").play("bgQuickFlash");
         if(gameState == GAME_STARTED)
         	score++;
         else
         	console.log("no score, wrong gamestate");

      });
         //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_green}", "click", function(sym, e) {
         // insert code for mouse click here
         stageRef.getSymbol("Gamebg").play("bgQuickFlash");
         if(gameState == GAME_STARTED)
         	score++;
         else
         	console.log("no score, wrong gamestate");

      });
         //Edge binding end

      

      })("bonus");
   //Edge symbol end:'bonus'

   //=========================================================
   
   //Edge symbol: 'hear'
   (function(symbolName) {   
   
      Symbol.bindTimelineAction(compId, symbolName, "Default Timeline", "play", function(sym, e) {
         // insert code to be run at timeline play here
         
      });
      //Edge binding end

   })("heart");
   //Edge symbol end:'heart'

   //=========================================================
   
   //Edge symbol: 'Instructions_1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         scoreList.deleteSymbol();
         gameState = GAME_INIT;
         console.log("deletedsymbol");
               stageRef.getSymbol("startMenu").playReverse();
         //sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_txtReturn}", "click", function(sym, e) {
         // insert code for mouse click here
         hideScores();

      });
      //Edge binding end

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         $.getJSON( API_URL+"?api=get", function( data ) {
           var items = [];
           $.each( data, function( i, ns ) {
         	 items.push( ns[1] + " : " + ns[0]);
           });
          	// Change an Element's contents.
          	//  (sym.$("name") resolves an Edge Animate element name to a DOM
          	//  element that can be used with jQuery)
          	sym.$("txtScores").html(items.join("<br />"));
         
           /*$( "<ul/>", {
         	 "class": "my-new-list",
         	 html: items.join( "" )
           }).appendTo( "body" );*/
         });
         

      });
      //Edge binding end

      })("scores");
   //Edge symbol end:'scores'

   //=========================================================
   
   //Edge symbol: 'intro'
   (function(symbolName) {   
   
   })("intro");
   //Edge symbol end:'intro'

   //=========================================================
   
   //Edge symbol: 'blink'
   (function(symbolName) {   
   
   })("blink");
   //Edge symbol end:'blink'

   //=========================================================
   
   //Edge symbol: 'circle_1'
   (function(symbolName) {   
   
      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         // insert code to be run when the symbol is created here
         circleRef = sym;

      });
         //Edge binding end

      })("into_circles");
   //Edge symbol end:'into_circles'

})(jQuery, AdobeEdge, "EDGE-728156"); 